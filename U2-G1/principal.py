#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Se llaman e importan las librerías necesarias para la construcción
# del código
import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


# Clases y funciones
class MainWindows():
    # Se define función de constructor
    def __init__(self):
        # Variable constructor
        builder = Gtk.Builder()
        # Se añade el archivo .ui desde la carpeta que lo aloja
        builder.add_from_file("U2-G1/U2-G1/ui/win.ui")
        # Se recupera la ventana con ID 'qwerty'
        win = builder.get_object("qwerty")
        # La ventana se designa como
        win.set_title("Ventana Principal G1-U2")
        # Al pulsar en el botón 'close' cierra completamente la actividad
        win.connect("destroy", Gtk.main_quit)
        # Se redimensiona el tamaño de la ventana
        win.resize(800, 600)

        # Creación de botones y textos
        # Botones
        self.btn_accept = builder.get_object("btn_accept")
        self.btn_accept.connect("clicked", self.accept)
        self.btn_reset = builder.get_object("btn_reset")
        self.btn_reset.connect("clicked", self.delete)
        # Textos y/o números
        self.alfa_text = builder.get_object("box_one")
        self.beta_text = builder.get_object("box_two")
        self.nro = builder.get_object("numeros")

        # Configuran acción de ventanas de diálogo
        self.btn_accept.connect("clicked", self.msg_dlg)
        self.btn_reset.connect("clicked", self.msg_dlg_wrg)
        # Se calcula la suma
        self.alfa_text.connect("activate", self.calc)
        self.beta_text.connect("activate", self.calc)

        # Muestra todo el contenido de la ventana
        win.show_all()

    # Función que calcula el largo de las cadenas

    def calcular(self, btn=None):
        self.talfa = self.alfa_text.get_text()
        self.tbeta = self.beta_text.get_text()

        try:
            lrga = int(self.talfa)
            lrgb = int(self.tbeta)

            ttlrg = lrga + lrgb
            self.nro.set_value(ttlrg)

        except ValueError:
            lrga = len(self.talfa)
            lrgb = len(self.tbeta)

            ttlrg = lrga + lrgb
            self.nro.set_value(ttlrg)

    # Función borrar y reiniciar datos

    def delete(self, btn=None):
        self.nro.set_value(0)
        self.alfa_text.set_text("")
        self.beta_text.set_text("")

    # Función calculo

    def msg_dlg(self, btn=None):
        self.calc()
        dlg_info(self.alfa_text, self.beta_text, self.nro)

    def msg_dlg_wrg(self, btn=None):
        # Mostrar ventana de diálogo
        dlg_wrg(self.alfa_text, self.beta_text, self.nro)


class dlg_info:
    def __init__(self.alfa_text, self.beta_text, self.nro)
    self.builder = Gtk.Builder()
    self.builder.add_from_file("U2-G1/U2-G1/ui/wws-accept.ui")


class dlg_wrg:
    def __init__(self.alfa_text, self.beta_text, self.nro)
    self.builder = Gtk.Builder()
    self.builder.add_from_file("U2-G1/U2-G1/ui/wws-reset.ui")


if __name__ == "__main__":
    MainWindows()
    Gtk.main()
